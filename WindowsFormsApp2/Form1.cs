﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            chart1.Series[0].Points.Clear();
            if (op.ShowDialog()==DialogResult.OK)
            {
                dataGridView1.Rows.Clear();
                LogReader.CheckLogsForAttack(op.FileName).ForEach(attacker =>  addRow(attacker.IPAddress , attacker.logEntries.Count.ToString()));


            }
        }

        private void addRow(string ip, string count)
        {
            dataGridView1.Rows.Add();
            dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[0].Value = dataGridView1.Rows.Count;
            dataGridView1.Rows[dataGridView1.Rows.Count-1].Cells[1].Value = ip;
            dataGridView1.Rows[dataGridView1.Rows.Count-1].Cells[2].Value = count;
            chart1.Series[0].Points.AddY(count);
        }
    }
}
