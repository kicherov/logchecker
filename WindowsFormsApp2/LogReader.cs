﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;

namespace WindowsFormsApp2
{
    class LogReader
    {
        public static List<IPSummary> CheckLogsForAttack(String logPath, bool filtered = true)
        {
            List<String> lines = File.ReadAllLines(logPath).ToList();
            lines.RemoveAt(0);
            lines.RemoveAt(lines.Count - 1);
            List<IPSummary> logEntries = lines
                .AsParallel().AsOrdered()
                .Select(line => line.Split('\t'))
                .Select(array =>
                    new LogEntry { DateTime = DateTime.Parse(array[0], CultureInfo.CreateSpecificCulture("en-US")), IPAddress = array[4] }
                )
                .Aggregate(new List<IPSummary>(), (acc, item) => {
                    var existingSummary = acc.FirstOrDefault(summary => summary.IPAddress.Equals(item.IPAddress));
                    if (existingSummary != null)
                        existingSummary.logEntries.Add(item);
                    else
                        acc.Add(new IPSummary { IPAddress = item.IPAddress, logEntries = new List<LogEntry> { item } });
                    return acc;
                });

            return (filtered)
                ? logEntries.FindAll(summary => summary.logEntries.Count > 100)
                : logEntries;
        }

        public class IPSummary
        {
            private String ipAddress;
            public String IPAddress
            {
                get
                {
                    return ipAddress;
                }

                set
                {
                    ipAddress = value;
                }
            }

            public List<LogEntry> logEntries = new List<LogEntry>();
        }

        public class LogEntry
        {
            private DateTime dateTime;
            public DateTime DateTime
            {
                get
                {
                    return dateTime;
                }

                set
                {
                    dateTime = value;
                }
            }

            private String ipAddress;
            public String IPAddress
            {
                get
                {
                    return ipAddress;
                }

                set
                {
                    ipAddress = value;
                }
            }
        }
    }
}
